package ca.landonjw.remoraids.api.commands.arguments.parsers;

import javax.annotation.Nonnull;
import java.util.Optional;

public interface IArgumentParser<T> {

    Optional<T> parse(@Nonnull String argument);

}
