package ca.landonjw.remoraids.implementation.listeners.pixelmon;

import ca.landonjw.remoraids.api.IBossAPI;
import ca.landonjw.remoraids.api.battles.IBossBattle;
import ca.landonjw.remoraids.api.boss.IBoss;
import ca.landonjw.remoraids.api.boss.IBossEntity;
import ca.landonjw.remoraids.implementation.boss.BossEntityRegistry;
import com.pixelmongenerations.api.events.PixelmonSendOutEvent;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.core.storage.PixelmonStorage;
import com.pixelmongenerations.core.storage.PlayerStorage;
import com.pixelmongenerations.core.util.PixelmonMethods;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.List;

/**
 * Listens for players sending out pixelmon, and if they are within range of a boss, will start battle with it.
 *
 * @author landonjw
 * @since  1.0.0
 */
public class EngageListener {

    /**
     * Invoked when a player sends out a pokemon.
     * This will start a boss battle if player is in range of one.
     *
     * @param event event caused player sending out pokemon
     */
    @SubscribeEvent
    public void onSendOut(PixelmonSendOutEvent event) {
        NBTLink nbtLink = new NBTLink(event.getTagCompound());

        if(nbtLink.getHealth() > 0){
            EntityPlayerMP player = event.getPlayer();

            for(IBossEntity bossEntity : BossEntityRegistry.getInstance().getAllBossEntities()){
                if(bossEntity.getBossEngager().isPlayerInRange(player)){
                    event.setCanceled(true);
                    IBoss boss = bossEntity.getBoss();

                    List<String> rejectionReasons = boss.getBattleSettings().getRejectionMessages(player, boss);
                    if (!rejectionReasons.isEmpty()) {
                        for (String reason : rejectionReasons) {
                            player.sendMessage(new TextComponentString(reason));
                        }
                        return;
                    } else {
                        IBossBattle battle = IBossAPI.getInstance().getBossBattleRegistry().getBossBattle(bossEntity).get();
                        PlayerStorage playerStorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player).get();
                        battle.startBattle(player, playerStorage.getPokemon(PixelmonMethods.getID(event.getTagCompound()), player.world));
                    }
                }
            }
        }
    }

}