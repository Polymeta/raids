package ca.landonjw.remoraids.implementation.battles.controller;

import ca.landonjw.remoraids.RemoRaids;
import ca.landonjw.remoraids.api.battles.IBossBattle;
import ca.landonjw.remoraids.implementation.battles.controller.participants.BossParticipant;
import ca.landonjw.remoraids.internal.config.RestraintsConfig;
import ca.landonjw.remoraids.internal.pokemon.PokemonUtils;
import com.pixelmongenerations.common.battle.attacks.Attack;
import com.pixelmongenerations.common.battle.controller.participants.PixelmonWrapper;
import com.pixelmongenerations.common.battle.status.StatusBase;
import com.pixelmongenerations.common.battle.status.StatusType;
import com.pixelmongenerations.common.entity.pixelmon.Entity8HoldsItems;
import com.pixelmongenerations.common.entity.pixelmon.EntityPixelmon;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.util.List;

public class BossWrapper extends PixelmonWrapper {

    private BossParticipant owner;

    /**
     * Constructor for the boss wrapper.
     *
     * @param owner       participant that wrapper is being controlled from
     * @param pixelmon    the pixelmon to make into wrapper
     */
    public BossWrapper(@Nonnull BossParticipant owner, @Nonnull EntityPixelmon pixelmon){
        super(owner, PokemonUtils.clonePokemon(owner.getBossEntity().getBoss().getPokemon()).getNBT(), 0);
        this.owner = owner;
        super.setHealth(owner.getBossBattle().getHealth());
        super.setMaxHealth(owner.getBossBattle().getMaxHealth());
        System.out.println(getHealth());
        this.pokemon = pixelmon;
        try {
            Field activeField = Entity8HoldsItems.class.getDeclaredField("dropped");
            activeField.setAccessible(true);
            activeField.setBoolean(this.pokemon, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        removeDisabledMoves();
    }

    /**
     * Removes all disabled boss moves from the wrapper.
     *
     * If the boss has no remaining moves, it will use Tackle.
     */
    private void removeDisabledMoves(){
        List<String> disabledMoves = RemoRaids.getRestraintsConfig().get(RestraintsConfig.DISABLED_BOSS_MOVES);
        for(String move : disabledMoves){
            getMoveset().removeIf(attack -> attack.isAttack(move));
        }

        if(getMoveset().isEmpty()){
            getMoveset().add(new Attack("tackle"));
        }
    }

    @Override
    public void updateBattleDamage(int damage) {
        owner.getBossBattle().setBossHealth(owner.getBossBattle().getHealth()-damage, owner.bc.getPlayers().get(0).player);
    }

    /**
     * Checks if the boss cannot have a particular status
     *
     * @param status   status to check
     * @param opponent opponent applying the status
     * @return true if the boss cannot have the status, false if it can
     */
    @Override
    public boolean cannotHaveStatus(StatusBase status, PixelmonWrapper opponent){
        return cannotHaveStatus(status, opponent, false);
    }

    /**
     * Checks if the boss cannot have a particular status.
     *
     * @param status               status to check
     * @param opponent             opponent applying the status
     * @param ignorePrimaryOverlap has no effect
     * @return true if the boss cannot have the status, false if it can
     */
    @Override
    public boolean cannotHaveStatus(StatusBase status, PixelmonWrapper opponent, boolean ignorePrimaryOverlap){
        List<StatusType> disabledStatus = RemoRaids.getRestraintsConfig().get(RestraintsConfig.DISABLED_STATUSES);
        StatusType[] arrDisabledStatus = new StatusType[disabledStatus.size()];
        disabledStatus.toArray(arrDisabledStatus);

        if(status.type.isStatus(arrDisabledStatus)){
            return true;
        }
        return super.cannotHaveStatus(status, opponent, ignorePrimaryOverlap);
    }

    @Override
    public int getMaxHealth() {
        return owner.getBossBattle().getMaxHealth();
    }

    @Override
    public int getHealth() {
        return owner.getBossBattle().getHealth();
    }

    @Override
    public int getBaseHealth() {
        return getHealth();
    }

}
