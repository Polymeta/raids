package ca.landonjw.remoraids.implementation.rewards;

import ca.landonjw.remoraids.api.rewards.contents.IRewardContent;
import com.pixelmongenerations.common.entity.pixelmon.drops.DroppedItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.Vec3d;

public class RewardDroppedItem extends DroppedItem {

    private IRewardContent reward;

    public RewardDroppedItem(IRewardContent reward, int id) {
        super(reward.toItemStack(), id);
        this.reward = reward;
    }

    @Override
    public void giveItem(EntityPlayerMP player) {
        this.reward.give(player);
    }

    @Override
    public void drop(Vec3d position, EntityPlayerMP player) {
        this.reward.give(player);
    }

}