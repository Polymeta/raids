package ca.landonjw.remoraids.internal.pokemon;

import com.pixelmongenerations.common.entity.pixelmon.stats.links.NBTLink;
import com.pixelmongenerations.common.entity.pixelmon.stats.links.PokemonLink;

public class PokemonUtils {

    /**
     * Generates a new Pokemon with similar characteristics to another Pokemon.
     *
     * @param pokemon Pokemon to generate a clone of.
     *
     * @return A Pokemon with very similar characteristics to another Pokemon.
     */
    public static PokemonLink clonePokemon(PokemonLink pokemon){
        NBTLink clonedPokemon = new NBTLink(pokemon.getNBT().copy());
        clonedPokemon.setHealth(pokemon.getHealth());
        clonedPokemon.getMoveset().healAllPP();
        return clonedPokemon;
    }

}
